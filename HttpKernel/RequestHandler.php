<?php
/**
 * This file is part of FancyGuy Distribution Bundle.
 *
 * Copyright (c) 2015 Steve Buzonas <steve@fancyguy.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FancyGuy\Bundle\DistributionBundle\HttpKernel;

use FancyGuy\Application\AppKernel;
use FancyGuy\Application\AppCache;
use Symfony\Component\Debug\Debug;
use Symfony\Component\HttpKernel\HttpCache\Store;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpFoundation\Request;

class RequestHandler
{

    const ENV_PRODUCTION  = 'prod';
    const ENV_TEST        = 'test';
    const ENV_DEVELOPMENT = 'dev';

    private $kernel;
    private $request;

    public function getKernel()
    {
        return $this->kernel;
    }

    public function getRequest()
    {
        return $this->request;
    }

    public function setKernel(HttpKernelInterface $kernel)
    {
        $this->kernel = $kernel;
        return $this;
    }

    public function setRequest(Request $request)
    {
        $this->request = $request;
        return $this;
    }

    public function handle()
    {
        $response = $this->getKernel()->handle($this->getRequest());
        $response->send();
        $this->getKernel()->terminate($this->getRequest(), $response);
    }

    public static function createFromGlobals()
    {
        $debug = false;
        $environment = self::getEnvironment();
        if (self::ENV_PRODUCTION !== $environment) {
            Debug::enable();
            $debug = true;
        }

        $kernel = new AppKernel($environment, $debug);
        $kernel->loadClassCache();

        if (!$debug) {
            $store = new Store($kernel->getCacheDir());
            $kernel = new AppCache($kernel, $store);
        }

        $handler = new self();
        $handler->setKernel($kernel);
        $handler->setRequest(Request::createFromGlobals());

        return $handler;
    }

    public static function getEnvironment()
    {
        $environment = null;
        if (getenv('ENVIRONMENT')) {
            $environment = getenv('ENVIRONMENT');
        } else {
            if (in_array(@$_SERVER['REMOTE_ADDR'], array('127.0.0.1', 'fe80::1', '::1'))) {
                $environment = self::ENV_DEVELOPMENT;
            }
        }

        switch ($environment) {
        	case self::ENV_DEVELOPMENT:
            case self::ENV_TEST:
            case self::ENV_PRODUCTION:
                break;
            default:
                $environment = self::ENV_PRODUCTION;
        }

        return $environment;
    }
}
