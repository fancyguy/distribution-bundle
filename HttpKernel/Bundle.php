<?php
/**
 * This file is part of FancyGuy Distribution Bundle.
 *
 * Copyright (c) 2015 Steve Buzonas <steve@fancyguy.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FancyGuy\Bundle\DistributionBundle\HttpKernel;

use Symfony\Component\HttpKernel\Bundle\Bundle as BaseBundle;

class Bundle extends BaseBundle
{

    /**
     * Instantiates the dependent bundles
     * @return BundleCollection
     */
    public function getDependencies()
    {
    }
}
