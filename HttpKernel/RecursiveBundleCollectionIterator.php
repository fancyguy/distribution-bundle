<?php
/**
 * This file is part of FancyGuy Distribution Bundle.
 *
 * Copyright (c) 2015 Steve Buzonas <steve@fancyguy.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FancyGuy\Bundle\DistributionBundle\HttpKernel;

class RecursiveBundleCollectionIterator extends BundleCollectionIterator implements \RecursiveIterator
{
    public function hasChildren()
    {
        if ($this->current() instanceof Bundle) {
            $dependencies = $this->current()->getDependencies();
            return $dependencies && count($dependencies) > 0;
        }
        return false;
    }

    public function getChildren()
    {
        $dependencies = $this->current()->getDependencies();
        $itClass = $dependencies->getIteratorClass();
        $dependencies->setIteratorClass(__CLASS__);
        $it = $dependencies->getIterator();
        $dependencies->setIteratorClass($itClass);
        return $it;
    }
}
