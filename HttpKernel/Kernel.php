<?php
/**
 * This file is part of FancyGuy Distribution Bundle.
 *
 * Copyright (c) 2015 Steve Buzonas <steve@fancyguy.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FancyGuy\Bundle\DistributionBundle\HttpKernel;

use Symfony\Component\HttpKernel\Kernel as BaseKernel;

abstract class Kernel extends BaseKernel
{
    /**
     * @return BundleCollection
     */
    abstract public function getBundleCollection();

    public function registerBundles()
    {
        $bundles = $this->flattenDependencyTree($this->getBundleCollection());
        return $bundles->getArrayCopy();
    }

    private function flattenDependencyTree(BundleCollection $bundles)
    {
        $collection = new BundleCollection();
        $itClass = $bundles->getIteratorClass();
        $bundles->setIteratorClass('\FancyGuy\Bundle\DistributionBundle\HttpKernel\RecursiveBundleCollectionIterator');
        $it = new \RecursiveIteratorIterator($bundles->getIterator(), \RecursiveIteratorIterator::CHILD_FIRST);
        foreach ($it as $bundle) {
            if (!$collection->hasBundle($bundle)) {
                $collection->append($bundle);
            }
        }

        return $collection;
    }
}
