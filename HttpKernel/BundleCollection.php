<?php
/**
 * This file is part of FancyGuy Distribution Bundle.
 *
 * Copyright (c) 2015 Steve Buzonas <steve@fancyguy.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FancyGuy\Bundle\DistributionBundle\HttpKernel;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class BundleCollection extends \ArrayObject
{

    const BUNDLE_CLASS = '\Symfony\Component\HttpKernel\Bundle\Bundle';

    /**
     * @var string[] list of class names already in the collection
     */
    private $classes;

    /**
     * @param Bundle $... bundles to append to collection
     */
    public function __construct()
    {
        parent::__construct();
        $this->setIteratorClass('\FancyGuy\Bundle\DistributionBundle\HttpKernel\BundleCollectionIterator');
        $this->exchangeArray(func_get_args());
    }

    /**
     * @var string|Bundle bundle to check for existance
     * @return bool|null whether or not the class exists, null on error
     */
    public function hasBundle($bundle)
    {
        if (is_string($bundle)) {
            $class = $bundle;
        } else if ($bundle instanceof Bundle) {
            $class = get_class($bundle);
        } else {
            return null;
        }

        return in_array($class, $this->classes);
    }

    /**
     * {@inheritdoc}
     */
    public function exchangeArray($input)
    {
        list($classes, $bundles) = $this->filterUniqueClasses($input);
        $this->classes = $classes;
        return parent::exchangeArray($bundles);
    }

    /**
     * {@inheritdoc}
     */
    public function append($value)
    {
        if (!$value instanceof Bundle) {
            throw $this->invalidClassException();
        }
        if (!$this->hasBundle($value)) {
            return parent::append($value);
        }
        throw new \BadMethodCallException('Not implmented');
    }

    /**
     * {@inheritdoc}
     */
    public function offsetSet($index, $newval)
    {
        if (!$newval instanceof Bundle) {
            throw $this->invalidClassException();
        }
        if (null !==$index && $this->offsetExists($index)) {
            throw new \BadMethodCallException('Not implmented');
        }
        return parent::offsetSet($index, $newval);
    }

    public function offsetUnset($index)
    {
        if ($this->offsetExists($index)) {
            $this->classes = array_diff($this->classes, array(get_class($this->getOffset($index))));
        }
        return parent::offsetUnset($index);
    }

    private function filterUniqueClasses(array $bundles)
    {
        $classes = array();
        foreach ($bundles as $k => $bundle) {
            if (!$bundle instanceof Bundle) {
                throw $this->invalidClassException();
            }
            $class = get_class($bundle);
            if (in_array($class, $classes)) {
                unset($bundles[$k]);
            } else {
                $classes[] = $class;
            }
        }
        return array($classes, $bundles);
    }

    private function invalidClassException()
    {
        return new \InvalidArgumentException('All objects must be an instance of '.self::BUNDLE_CLASS.'.');
    }
}
