<?php
/**
 * This file is part of FancyGuy Distribution Bundle.
 *
 * Copyright (c) 2015 Steve Buzonas <steve@fancyguy.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FancyGuy\Bundle\DistributionBundle;

use FancyGuy\Bundle\DistributionBundle\HttpKernel\Bundle;
use FancyGuy\Bundle\DistributionBundle\HttpKernel\BundleCollection;

class FancyGuyDistributionBundle extends Bundle
{
    public function getDependencies()
    {
        return new BundleCollection(
            new \Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new \Symfony\Bundle\MonologBundle\MonologBundle()
        );
    }
}
